#include <SPI.h>
#include <math.h>

#define STORAGE_SIZE  256

int focuserPosition, targetPos, apValue, offset, apAddr, x, y;    
float Temp, Vpower, Vadc, Rt, Rm, Ro, betta;
String targetStr, apStr, gStr;
boolean IsMoving, IsFirstConnect;
char inStr[6];
int aperture = 0;

uint8_t miso[STORAGE_SIZE];
uint8_t mosi[STORAGE_SIZE];

// commands
uint8_t cmdLensInfo[]      = {0x80,0x00,0x00,0x00,0x00,0x00,0x00,0x00,};// I# Lens Info: Lens ID, Min, Max Zoom, Protocol version, Brand
uint8_t cmdLensReset[]     = {0x90, 0x8E, 0x00};                        // R#
uint8_t cmdInitLens[]      = {0x0A, 0x00, 0x0A, 0x00};                  // C#
uint8_t cmdFullOpen[]      = {0x13, 0x80};                              // O# Full Open Aperture
uint8_t cmdQueryAperture[] = {0xB0, 0x00, 0x00, 0x00};                  // Q# Query Min/Max Aperture
uint8_t cmdFocusPosition[] = {0x44, 0x00, 0x00, 0x00};                  // Mxxxx#
uint8_t cmdMoveNearest[]   = {0x06};                                    // N#
uint8_t cmdMoveFurthest[]  = {0x05};                                    // F#
uint8_t cmdBoot[]          = {0x09,0x00,0x0E,0x0F};                     // B#
uint8_t cmdQueryPosition[] = {0xC0, 0x00, 0x00};                        // P#
uint8_t cmdZoomValue[]     = {0xA0, 0x00, 0x00};                        // Z# Zoom value
uint8_t cmdSetDiaphram     = {0x12, 0x00}                               // Dxx# Command Diaphram

void spiarray(uint8_t mosi[], uint8_t miso[], int n ){
  Serial.print("Sent: ");
  Serial.print(n);
  Serial.print(" bytes. mosi:");
  for(int i; i<n; i++){
    Serial.print(" 0x");
    Serial.print(mosi[i],HEX);
    miso[i]=SPI.transfer(mosi[i]);
    delay(30);
  }
  Serial.print("  miso:");
  for(int i; i<n; i++){
    Serial.print(" 0x");
    Serial.print(miso[i],HEX);
  }
  Serial.println("");

}

void setup()
{
  apAddr = 0;
  focuserPosition = 5000;
  IsMoving = false;
  IsFirstConnect = true;
  digitalWrite(13, LOW);
  pinMode(2, OUTPUT);
  digitalWrite(2, HIGH); //pull-up pin for temp sensor
  
  //Temperature init
  Rm = 7400; //Ohm
  Ro = 3000; //Ohm
  Vpower = 4.73; //Volts
  betta = 3988.0;
  
  Serial.begin(115200);
  
  SPI.begin();
  SPI.setBitOrder(MSBFIRST);
  SPI.setClockDivider(SPI_CLOCK_DIV128); 
  SPI.setDataMode(SPI_MODE3);  
  digitalWrite(12, HIGH);
   
  spiarray(cmdInitLens,miso,sizeof(cmdInitLens));
  
}

void loop()
{
  if (Serial.available() > 0) 
  {         
    Serial.readBytesUntil('#', inStr, 6);
    
    // Get Position
    if (inStr[0] == 'P')
    {
      Serial.print(focuserPosition);
      Serial.print("#");
    }
    
    // Get Temperature
    if (inStr[0] == 'T')
    {
      Vadc = (analogRead(A7) / 1023.0) * 5;
      Rt = (Rm * Vadc) / (Vpower - Vadc);
      Temp = (betta / (log(Rt / Ro) + (betta / 298.0))) - 273.0;
      
      Serial.print(int(Temp * 100));
      Serial.print("#");
    }
    
    // Move
    if (inStr[0] == 'M')
    {
      targetStr = inStr;
      targetPos = (targetStr.substring(1, 5)).toInt();
      offset = targetPos - focuserPosition;

      cmdFocusPosition[1] = highByte(offset);
      cmdFocusPosition[2] = lowByte(offset); 

      Serial.print("offset: ");
      Serial.print(offset);
      Serial.print(" ");

      IsMoving = true;
      spiarray(cmdInitLens,miso,sizeof(cmdInitLens));
      spiarray(cmdFocusPosition,miso,sizeof(cmdFocusPosition));
      IsMoving = false;
      focuserPosition = targetPos;
    }
    
    // IsMoving
    if (inStr[0] == 'i')
    {
      if (IsMoving == true) {
        Serial.print("true#");
      }
      else {
        Serial.print("false#");
      }
    }
    
    if (inStr[0] == 'I')  {                                     // Lens ID info
      spiarray(cmdLensInfo,miso,sizeof(cmdLensInfo));
      Serial.print("Lens ID: ");
      Serial.print(miso[1],HEX);
      Serial.print(miso[2],HEX);
      Serial.print(" Min FL: ");
      Serial.print(miso[3]*256+miso[4]);
      Serial.print(" Max FL: ");
      Serial.println(miso[5]*256+miso[6]);
    }


    if (inStr[0] == 'R'){                                       // Lens Reset
      spiarray(cmdLensReset,miso,sizeof(cmdLensReset));
    }

    if (inStr[0] == 'O'){                                       // Lens Full Open Aperture
      spiarray(cmdFullOpen,miso,sizeof(cmdFullOpen));
    }

    if (inStr[0] == 'Q'){                                       // Query current lens Aperture
      spiarray(cmdQueryAperture,miso,sizeof(cmdQueryAperture));
    }

    if (inStr[0] == 'N'){                                       // Focus Nearest Point
      spiarray(cmdMoveNearest,miso,sizeof(cmdMoveNearest));
      focuserPosition = 0;
    }
    
    if (inStr[0] == 'F'){                                       // Focus Furthest Point
      spiarray(cmdMoveFurthest,miso,sizeof(cmdMoveFurthest));
    }

    if (inStr[0] == 'P'){                                       // Focus Furthest Point
      spiarray(cmdQueryPosition,miso,sizeof(cmdQueryPosition));
    }

    if (inStr[0] == 'Z'){                                       // Zoom Value
      spiarray(cmdZoomValue,miso,sizeof(cmdZoomValue));
    }

    if (inStr[0] == 'B'){                                       // Boot Reset?
      spiarray(cmdBoot,miso,sizeof(cmdBoot));
    }

    if (inStr[0] == 'D'){                                       // Boot Reset?
      apStr = inStr;
      apValue = (apStr.substring(1, 3)).toInt();
      cmdSetDiaphram[1] = lowByte(apValue);
      spiarray(cmdSetDiaphram,miso,sizeof(cmdSetDiaphram));
    }

    // Set Aperture
    if (inStr[0] == 'A')
    {
      apStr = inStr;
      apValue = (apStr.substring(1, 3)).toInt();
      
      if (apValue != aperture)
      { 
        //InitLens();
        spiarray(cmdInitLens,miso,sizeof(cmdInitLens));        
        
        SPI.transfer(0x07);
        delay(30);
        SPI.transfer(0x13);
        delay(30);
        SPI.transfer((apValue - aperture) * 3);
        delay(30);
        SPI.transfer(0x08);
        delay(30);
        SPI.transfer(0x00);
        delay(30);
        
        aperture = apValue;
      }
    }
  }  
}
